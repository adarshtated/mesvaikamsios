//
//  APIHandler.swift
//  MesVaikams
//
//  Created by anushka mishra on 19/06/20.
//  Copyright © 2020 Osiyatech. All rights reserved.
//

//
//  ApiHandler.swift
//  MyWaiter
//
//  Created by Apple on 15/02/19.
//  Copyright © 2019 Sejal. All rights reserved.
//
import UIKit
import Foundation
import AVFoundation
import AVKit
//= == =============Api call Without image === ================

let APPPINKCOLOR = UIColor(red: 232/255, green: 81/255, blue: 19/255, alpha: 1.0)
let APPPBACKCOLOR = UIColor(red: 31/255, green: 33/255, blue: 36/255, alpha: 1.0)
let baseUrl = "http://192.168.1.130/mesvaikams/api/app/"
struct Root: Codable {
    let  data: [InnerItem]
}
struct InnerItem:Codable {
    let  id: Int?
    let  image: String?
    let  name: String?
    
    private enum CodingKeys : String, CodingKey {
        case id = "id", image = "image", name = "name"
    }
}

public func
    postWithoutImage(url_string: String, parameters : String, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    
    print("parameters -",parameters)
    let url = URL(string: baseUrl+url_string)!
    print("url : ",url)
    var request = URLRequest(url: url)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    
    if parameters != "" {
        request.httpBody = parameters.data(using: .utf8)
        request.addValue("apple_ghjkl_planet_5541236", forHTTPHeaderField: "Authtoken")
    }
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        UIApplication.shared.keyWindow?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                        
                    }
                }
            }
            return
        }
        
        let string = String(data: data, encoding: String.Encoding.utf8)
        //print(string) //JSONSerialization
        
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            //print("statusCode should be 200, but is \(httpStatus.statusCode)")
            //print("response = \(response!)")
            DispatchQueue.main.async {
                UIApplication.shared.keyWindow?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                //                UIApplication.shared.keyWindow?.activityStopAnimating()
                
            }
            return
        }
        
        DispatchQueue.main.async {
            if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
                    //  print("json of ---\(url_string) :",json)
                    let statusmessage1 = String(describing: json["status"])
                    let msg = json.value(forKey: "message") as! String
                    if (statusmessage1.contains("2000")){
                        print("success")
                        CompletionHandler(json, nil)
                    } else {
                        print("false")
                        CompletionHandler(nil, msg)
                    }
                }catch{
                    //print("data~~ \(data) ~~~~ response === \(String(describing: response))")
                    //print("error on decode: \(error.localizedDescription) \n \(error)\n\n")
                    //                    {
                    ///////
                    
                    let currentDate: Date = Date()
                    let stringDate: String = currentDate.app_stringFromDate()
                    var errorArr = [String : NSDictionary]()
                    let item1 = NSMutableDictionary()
                    item1.setObject("ios", forKey: "from" as NSCopying)
                    item1.setObject(error, forKey: "error" as NSCopying)
                    item1.setObject(stringDate, forKey: "date_time" as NSCopying)
                    item1.setObject(baseUrl+url_string, forKey: "api_name" as NSCopying)
                    errorArr["json"] = item1
                    print("errorArr ",errorArr)
                    if let theJSONData = try?  JSONSerialization.data(withJSONObject: errorArr,options: .prettyPrinted),
                        let theJSONText = String(data: theJSONData,encoding: String.Encoding.ascii) {
                        //                        jsonData = theJSONText.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
                        let jsondata = theJSONText
                        print("save exception jsondata ~~~~~~",jsondata)
                        callsaveExceptionLogApi(parameters: "app_side=ios&json=\(jsondata)", CompletionHandler: { json, error in
                            if let json = json {
                                print("save exception final json ~~~~~~~",json)
                            }
                        })
                    }
                    
                    //////
                    //                    }
                    
                }
            }
        }
        DispatchQueue.main.async {
            
        }
    }
    task.resume()
}

//post method without parameters
public func postMethod(url_string: String, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    // let baseUrl = "https://adarshtated.com/dev/apple_planet_dev/api/"
    
    let url = NSURL(string: baseUrl+url_string)
    let session = URLSession.shared
    let request = NSMutableURLRequest(url: url! as URL)
    print("url - - - ",url!)
    request.httpMethod = "POST"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    request.addValue("apple_ghjkl_planet_5541236", forHTTPHeaderField: "Authtoken")
    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        UIApplication.shared.keyWindow?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                        
                        
                    }
                }
            }
            return
        }
        let string = String(data: data, encoding: String.Encoding.utf8)
        
        DispatchQueue.main.async {
            if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! NSDictionary
                    // print("json of --- \(url_string) :",json)
                    let statusmessage1 = String(describing: json["status"])
                    let msg = json.value(forKey: "message") as? String ?? ""
                    if (statusmessage1.contains("2000")){
                        print("success")
                        CompletionHandler(json, nil)
                    } else {
                        print("false")
                        CompletionHandler(nil, msg)
                    }
                }catch{
                    print("data~~ \(data) ~~~~ response === \(String(describing: response))")
                    print("error on decode: \(error.localizedDescription) \n \(error)\n\n")
                    let currentDate: Date = Date()
                    let stringDate: String = currentDate.app_stringFromDate()
                    var sendreq: [AnyHashable : Any] = [:]
                    
                    sendreq = ["from": "ios","error": "\(error)","date_time": "\(stringDate)","api_name": "\(baseUrl+url_string)"]
                    var _: Error? = nil
                    var jsonData: Data? = nil
                    do {
                        jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
                    } catch {
                    }
                    var jsonString: String? = nil
                    if let jsonData = jsonData {
                        jsonString = String(data: jsonData, encoding: .utf8)
                    }
                    callsaveExceptionLogApi(parameters: "app_side=ios&json=\(jsonString ?? "")", CompletionHandler: { json, error in
                        if let json = json {
                            print("save exception final json ~~~~~~~",json)
                        }
                    })
                }
            }
        }
        DispatchQueue.main.async {
            
            
        }
    })
    task.resume()
}


public func postWithoutImageWithStatus(url_string: String, parameters : String, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    
    print("parameters -",parameters)
    //https://adarshtated.com/dev/swamiji/api/Registration
    let url = URL(string: baseUrl+url_string)!
    print("url : ",url)
    var request = URLRequest(url: url)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    if parameters != "" {
        print("parameters not empty")
        request.httpBody = parameters.data(using: .utf8)
        request.addValue("swamiji", forHTTPHeaderField: "Authtoken")
    }
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        UIApplication.shared.keyWindow?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                        
                        
                    }
                }
            }
            return
        }
        let string = String(data: data, encoding: String.Encoding.utf8)
        // print(string) //JSONSerialization
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            //print("response = \(response!)")
            DispatchQueue.main.async {
                
                
            }
            return
        }
        DispatchQueue.main.async {
            if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
                    print("json of ---\(url_string) :",json)
                    let statusmessage1 = String(describing: json["status"])
                    let msg = json.value(forKey: "message") as! String
                    if (statusmessage1.contains("2000")){
                        print("success")
                        CompletionHandler(json, nil)
                    } else {
                        print("false")
                        CompletionHandler(json, msg)
                    }
                }catch{
                    print("data~~ \(data) ~~~~ response === \(String(describing: response))")
                    print("error on decode: \(error.localizedDescription) \n \(error)\n\n")
                    let currentDate: Date = Date()
                    let stringDate: String = currentDate.app_stringFromDate()
                    var sendreq: [AnyHashable : Any] = [:]
                    
                    sendreq = ["from": "ios","error": "\(error)","date_time": "\(stringDate)","api_name": "\(baseUrl+url_string)"]
                    var _: Error? = nil
                    var jsonData: Data? = nil
                    do {
                        jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
                    } catch {
                    }
                    var jsonString: String? = nil
                    if let jsonData = jsonData {
                        jsonString = String(data: jsonData, encoding: .utf8)
                    }
                    callsaveExceptionLogApi(parameters: "app_side=ios&json=\(jsonString ?? "")", CompletionHandler: { json, error in
                        if let json = json {
                            print("save exception final json ~~~~~~~",json)
                        }
                    })
                }
            }
        }
        DispatchQueue.main.async {
            
            
        }
    }
    task.resume()
}


public func
    callsaveExceptionLogApi(parameters : String, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    
    print("parameters -",parameters)
    let baseUrl = "https://adarshtated.com/dev/apple_planet/api/saveException"
    let url = URL(string: baseUrl)!
    print("url : ",url)
    var request = URLRequest(url: url)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    
    if parameters != "" {
        request.httpBody = parameters.data(using: .utf8)
        request.addValue("apple_ghjkl_planet_5541236", forHTTPHeaderField: "Authtoken")
    }
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        UIApplication.shared.keyWindow?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                        
                    }
                }
            }
            return
        }
        
        let string = String(data: data, encoding: String.Encoding.utf8)
        //print(string) //JSONSerialization
        
        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            //print("response = \(response!)")
            DispatchQueue.main.async {
                //                UIApplication.shared.keyWindow?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                
                
            }
            return
        }
        
        DispatchQueue.main.async {
            if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
                    print("json of saveException api :",json)
                    let statusmessage1 = String(describing: json["success"])
                    if (statusmessage1.contains("true")){
                        print("success")
                        CompletionHandler(json, nil)
                    } else {
                        print("false")
                        CompletionHandler(json, nil)
                    }
                }catch{
                    //print("data~~\(data)~~~~response===\(String(describing: response))")
                    print("error on decode: \(error.localizedDescription)")
                }
            }
        }
        DispatchQueue.main.async {
            
            
        }
    }
    task.resume()
}




public func postWithImage(url_string: String,parameters: [String: String], imgParaName: String, imageArr: [UIImage], sigParaName: String, sigImage: UIImage?, del_sigParaName: String, del_sigImage: UIImage?, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    // let baseUrl = "https://adarshtated.com/dev/apple_planet/api/"
    
    let url = URL(string: baseUrl+url_string)
    var imgData = [Data]()
    for i in 0..<imageArr.count{
        imgData.append(imageArr[i].jpegData(compressionQuality: 0.1)!)
    }
    
    let imgData1 = sigImage?.jpegData(compressionQuality: 0.1)!
    print("imgData~~~~~~",imgData)
    print("imgData1~~~~~~",imgData1)
    
    let imgData3 = del_sigImage?.jpegData(compressionQuality: 0.1)!
    
    let sessionConfig = URLSessionConfiguration.default
    sessionConfig.timeoutIntervalForRequest = 60.0
    sessionConfig.timeoutIntervalForResource = 60.0
    let session = URLSession(configuration: sessionConfig)
    
    print("url: ",url!,"parameters: ",parameters)
    var request = URLRequest(url: url!)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    if parameters != [:] {
        let boundary = "Boundary-\(NSUUID().uuidString)"
        
        request.httpBody = createBodyWithParametersImage(parameters: parameters, filePathKey1: imgParaName, imageDataKey1: imgData as [Data], filePathKey2: sigParaName, imageDataKey2: imgData1, filePathKey3: del_sigParaName, imageDataKey3: imgData3, boundary: boundary) as Data
        request.addValue("apple_ghjkl_planet_5541236", forHTTPHeaderField: "Authtoken")
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        //        request.setValue("text/html; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("multipart/form-data", forHTTPHeaderField: "Accept")
        
    }
    
    // manager.session.configuration.timeoutIntervalForRequest = 120
    
    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        guard let data = data, error == nil else {
            DispatchQueue.main.async {
                if let err = error as NSError? {
                    if err.code == -1009{
                        UIApplication.shared.keyWindow?.rootViewController?.alert(message: "Network error. Please try again.", title: "Alert!")
                        
                        
                    }
                }
            }
            return
        }
        
        let string = String(data: data, encoding: String.Encoding.utf8)
        
        
        //        print("data - ",data as Any,response as Any)
        if let data = string?.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                    //print("json of \(url_string) api is - - - ",json)
                    let status = json.value(forKey: "status") as? Int ?? 0
                    let msg = json.value(forKey: "message") as! String
                    print("status - ",status)
                    if status == 2000{
                        print("success")
                        CompletionHandler(json, msg)
                    } else {
                        print("failed")
                        CompletionHandler(json, msg)
                    }
                }
            } catch let error {
                DispatchQueue.main.async {
                    
                    
                }
                
                print("data~~ \(data) ~~~~ response === \(String(describing: response))")
                print("error on decode: \(error.localizedDescription) \n \(error)\n\n")
                let currentDate: Date = Date()
                let stringDate: String = currentDate.app_stringFromDate()
                var sendreq: [AnyHashable : Any] = [:]
                
                sendreq = ["from": "ios","error": "\(error)","date_time": "\(stringDate)","api_name": "\(baseUrl+url_string)"]
                var _: Error? = nil
                var jsonData: Data? = nil
                do {
                    jsonData = try JSONSerialization.data(withJSONObject: sendreq, options: .prettyPrinted)
                } catch {
                }
                var jsonString: String? = nil
                if let jsonData = jsonData {
                    jsonString = String(data: jsonData, encoding: .utf8)
                }
                callsaveExceptionLogApi(parameters: "app_side=ios&json=\(jsonString ?? "")", CompletionHandler: { json, error in
                    if let json = json {
                        print("save exception final json ~~~~~~~",json)
                    }
                })
            }
        }
        DispatchQueue.main.async {
            
            
        }
    })
    task.resume()
}

public func createBodyWithParametersImage(parameters: [String: String]?, filePathKey1: String?, imageDataKey1: [Data], filePathKey2: String?, imageDataKey2: Data?, filePathKey3: String?, imageDataKey3: Data?, boundary: String) -> NSData {
    //    let body = NSMutableData();
    //    print("@@",filePathKey1,"\n",imageDataKey1)
    //    if parameters != nil {
    //        for (key, value) in parameters! {
    //            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
    //            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
    //            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
    //        }
    //    }
    //    print("~~~~~~~~~~ createBodyWithImage")
    //    let filename1 = "attachImage.jpg"
    //    let filename2 = "attachImage1.jpg"
    //    let mimetype1 = "image/jpg"
    //    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
    //    body.append("Content-Disposition: form-data; name=\"image[]\"; filename=\"\(filename1)\"\r\n".data(using: String.Encoding.utf8)!)
    //    body.append("Content-Type: \(mimetype1)\r\n\r\n".data(using: String.Encoding.utf8)!)
    //    for i in 0..<imageDataKey1.count{
    //        body.append(imageDataKey1[i] as Data)
    //    }
    //
    //    body.append("\r\n".data(using: String.Encoding.utf8)!)
    //    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    //
    //    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
    //    body.append("Content-Disposition: form-data; name=\"signature\"; filename=\"\(filename2)\"\r\n".data(using: String.Encoding.utf8)!)
    //    body.append("Content-Type: \(mimetype1)\r\n\r\n".data(using: String.Encoding.utf8)!)
    //    body.append(imageDataKey2 as Data)
    //    body.append("\r\n".data(using: String.Encoding.utf8)!)
    //    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    //
    //    return body
    
    let body = NSMutableData();
    if parameters != nil {
        for (key, value) in parameters! {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
    }
    
    print("filePathKey1: \(filePathKey1) filePathKey2:\(filePathKey2)")
    for i in 0..<imageDataKey1.count{
        let randomInt = Int.random(in: 1...10000000)
        let imgData = imageDataKey1[i]
        print("image data---", imgData)
        let mimetype1 = "image/jpg"
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition: form-data; name=\"\(filePathKey1!)\"; filename=\"\(randomInt)img.jpg\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype1)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(imgData as Data)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    }
    if (imageDataKey2 != nil) {
        let randomInt = Int.random(in: 2100...100000)
        let mimetype1 = "image/jpg"
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition: form-data; name=\"\(filePathKey2!)\"; filename=\"\(randomInt)sigImg.jpg\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype1)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(imageDataKey2!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    }else{
        let randomInt = Int.random(in: 2100...100000)
        let imgData123:Data = Data()
        let mimetype1 = "image/jpg"
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition: form-data; name=\"\(filePathKey2!)\"; filename=\"\(randomInt)sigImg.jpg\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype1)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(imgData123)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    }
    if (imageDataKey3 != nil) {
        let randomInt = Int.random(in: 1100...100000)
        let mimetype1 = "image/jpg"
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition: form-data; name=\"\(filePathKey3!)\"; filename=\"\(randomInt)delSig.jpg\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype1)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(imageDataKey3!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    }else{
        let randomInt = Int.random(in: 1100...100000)
        let imgData123:Data = Data()
        let mimetype1 = "image/jpg"
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition: form-data; name=\"\(filePathKey3!)\"; filename=\"\(randomInt)delSig.jpg\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype1)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(imgData123)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    }
    return body
    
}

public func createBodyWithParameters(parameters: [String: Any]) -> NSData {
    let body = NSMutableData();
    let boundary = "Boundary-\(NSUUID().uuidString)"
    for (key, value) in parameters {
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
    }
    return body
}
//= == =============TextField round === ================
public func roundTextFields(textField:UITextField){
    textField.layer.cornerRadius = textField.frame.height/2
    textField.clipsToBounds = true
    textField.layer.borderWidth = 2
    textField.layer.borderColor = UIColor(displayP3Red: 255/0, green: 255/127, blue: 255/192, alpha: 1).cgColor
    
}

//= == =============TextField Padding === ================
public func txtPaddingVw(txt:UITextField) {
    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 5))
    txt.leftViewMode = .always
    txt.leftView = paddingView
}

//================ extension for alert view ===================
extension UIViewController {
    func alert(message: String, title: String ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}


//================ extension for Date ===================
extension Date {
    func app_stringFromDate() -> String{
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let strdt = dateFormatter.string(from: self as Date)
        if let dtDate = dateFormatter.date(from: strdt){
            return dateFormatter.string(from: dtDate)
        }
        return "--"
    }
}
