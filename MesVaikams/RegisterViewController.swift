//
//  RegisterViewController.swift
//  MesVaikams
//
//  Created by anushka mishra on 19/06/20.
//  Copyright © 2020 Osiyatech. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet var fieldUserName: UITextField!
    @IBOutlet var fieldEmail: UITextField!
    @IBOutlet var fieldPassword: UITextField!
    @IBOutlet var fieldMobile: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClcikButton(_ sender: UIButton) {
        if sender.tag == 1{
            if fieldEmail.text == "" && fieldPassword.text == "" && fieldUserName.text == "" && fieldMobile.text == "" {
                self.alert(message: "Please enter user name,email,password & mobile", title: "Alert")
            }else{
                callRegister()
            }
        }else if sender.tag == 2{
            let next = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.present(next, animated: true, completion: nil)
        }
    }
    
    
    func callRegister(){
        postWithoutImage(url_string: "registration", parameters: "username=\(fieldUserName.text ?? "")&email=\(fieldEmail.text ?? "")&password=\(fieldPassword.text ?? "")&user_nicename=&phone=\(fieldMobile.text ?? "")",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let next = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.present(next, animated: true, completion: nil)
                }else{
                    self.alert(message: error!, title: "Alert!")
                }
            }
        })
    }

}
