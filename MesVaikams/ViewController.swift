//
//  ViewController.swift
//  MesVaikams
//
//  Created by anushka mishra on 18/06/20.
//  Copyright © 2020 Osiyatech. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class ViewController: UIViewController {
    
    @IBOutlet var fieldEmail: UITextField!
    @IBOutlet var fieldPassword: UITextField!
    
    @IBOutlet var fieldForgotEmail: UITextField!
    @IBOutlet var forgotPopupView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        let loginButton = FBLoginButton()
//        loginButton.center = view.center
//        view.addSubview(loginButton)
//        loginButton.permissions = ["public_profile", "email"]
//
//        if let token = AccessToken.current,
//            !token.isExpired {
//            print("AccessToken.current~~~~",AccessToken.current)
//            // User is logged in, do work such as go to next view controller.
//        }
    }

    @IBAction func onClickButton(_ sender: UIButton) {
        if sender.tag == 1{
            
            let next = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            self.present(next, animated: true, completion: nil)
            
//            if fieldEmail.text == "" && fieldPassword.text == "" {
//                self.alert(message: "Please enter email & password", title: "Alert")
//            }else{
//                callLogIn()
//            }
        }else if sender.tag == 2{
            let next = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
            self.present(next, animated: true, completion: nil)
        }else if sender.tag == 3{
            forgotPopupView.isHidden = false
        }else if sender.tag == 4{
            
        }else if sender.tag == 5 || sender.tag == 6{
            forgotPopupView.isHidden = true
        }
    }
    
    func callLogIn(){
        postWithoutImage(url_string: "user_login", parameters: "username=\(fieldEmail.text ?? "")&password=\(fieldPassword.text ?? "")",CompletionHandler: { json, error in
            if let json = json {
                print("json~~~~~~~",json)
                let statusmassege = json.value(forKey: "status") as! Bool
                if (statusmassege == true){
                    let next = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    self.present(next, animated: true, completion: nil)
                }else{
                    self.alert(message: error!, title: "Alert!")
                }
            }
        })
    }
    
}

