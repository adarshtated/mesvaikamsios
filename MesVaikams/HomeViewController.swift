//
//  HomeViewController.swift
//  MesVaikams
//
//  Created by anushka mishra on 18/06/20.
//  Copyright © 2020 Osiyatech. All rights reserved.
//

import UIKit
import WebKit

class HomeViewController: UIViewController,WKUIDelegate,WKNavigationDelegate {

    @IBOutlet var webView: WKWebView!
    @IBOutlet var actIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        actIndicator.startAnimating()
        webView.uiDelegate = self
        webView.navigationDelegate = self
        let url = URL(string: "https://www.mesvaikams.lt/?callWeb=AppSide")
        let request = URLRequest(url: url!)
        webView.load(request)

        // Do any additional setup after loading the view.
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        actIndicator.stopAnimating()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
